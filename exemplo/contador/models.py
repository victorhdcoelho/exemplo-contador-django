from django.db import models


class Contador(models.Model):
    id_contador = models.AutoField(primary_key=True)
    ds_categoria = models.TextField()
    qtd_contador = models.IntegerField(default=0)

    def __str__(self):
        return self.ds_categoria + " | " + str(self.qtd_contador)
