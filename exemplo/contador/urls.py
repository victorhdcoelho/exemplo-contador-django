from django.conf.urls import url
from contador.views import add_view


urlpatterns = [
    url('add_contador', add_view, name="add_view"),
]
