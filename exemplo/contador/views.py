from contador.models import Contador
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
@require_http_methods(["POST"])
def add_view(request):
    data = request.POST
    contador = Contador.objects.get(ds_categoria=data["categoria"])
    contador.qtd_contador += 1
    contador.save()
    return JsonResponse({"classe": contador.ds_categoria,
                         "qtd": contador.qtd_contador})
